package com.company;

import Game.*;

public class Main {

    public static void main(String[] args) {

        Roles rols = new King("BOSS");
        //doAttack(rols);
        doWork(rols);

        Roles rolsG = new Giant("BIG");
        doWork(rolsG);

        Roles rolesS = new Shooter("Gun");
        doWork(rolesS);

        Roles rolesW = new Warrior("Magic");
        doWork(rolesW);

        Roles rolesJ = new Juggernaut("Swordsman");
        doWork(rolesJ);

        Roles rolesC = new Chef("Baker");
        doWork(rolesC);

    }
    public static  void doAttack(IActions iActions){
        iActions.doAttack();
    }
    public static  void doWork(Roles rols){
        System.out.println(rols);
        rols.doAttack();
        //rols.doAttack("石頭");
        rols.doSpecialAttack();
    }
}
