package Game;

public class Juggernaut extends Roles {
    public Juggernaut(String name){
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("Juggernaut名字是"+getName()+"，我拿劍砍人");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("Juggernaut名字是"+getName()+"，我拿"+weapon+"砍人");
    }

    @Override
    public void doMagic() {

    }

    @Override
    public void doMagic(String magic) {

    }

    @Override
    public void doSpecialAttack() {
        System.out.println("Juggernaut名字是"+getName()+"，我揮舞著大劍砍人");
    }
}
