package Game;

public class Giant extends Roles {
    public Giant(String name){
        super(name);
    }
    @Override
    public void doAttack() {
        System.out.println("Giant名字是"+getName()+"，我用狼牙棒打人");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("Giant名字是"+getName()+"，我用"+ weapon +"打人");
    }

    @Override
    public void doMagic() {

    }

    @Override
    public void doMagic(String magic) {

    }

    @Override
    public void doSpecialAttack() {
        System.out.println("Giant名字是"+getName()+"，我使用泰山壓頂壓人");
    }
}
