package Game;

public class Shooter extends Roles {

    public Shooter(String name){
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("Shooter名字是"+getName()+"，我用手槍斃人");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("Shooter名字是"+getName()+"，我用"+ weapon +"斃人");
    }

    @Override
    public void doMagic() {

    }

    @Override
    public void doMagic(String magic) {

    }

    @Override
    public void doSpecialAttack() {
        System.out.println("Shooter名字是"+getName()+"，我亂槍打鳥打倒一切敵人");
    }
}
