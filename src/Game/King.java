package Game;

public class King extends Roles {

    public King(String name){
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("King名字是"+getName()+"，我用錢砸人");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("King名字是"+getName()+"，我用"+ weapon +"砸人");
    }

    @Override
    public void doMagic() {

    }

    @Override
    public void doMagic(String magic) {

    }

    //@Override
    public void doSpecialAttack() {
        System.out.println("King名字是"+getName()+"，我用軍隊打壓人民");
    }
}
