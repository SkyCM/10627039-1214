package Game;

import java.util.List;

public abstract class Roles implements IActions {

    private String name;
    private int level;
    private int exp;
    private int hp;
    private int mp;
    private List<String> equipments;
    private String weapon;
    private String armor;

    public Roles(String name,int level , int exp , int hp , int mp ){
        this.name = name;
        this.level = level;
        this.exp = exp;
        this.hp = hp;
        this.mp = mp;
    }

    public Roles(String name){
        this(name,1,10,10,10);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public List<String> getEquipments() {
        return equipments;
    }

    public void setEquipments(List<String> equipments) {
        this.equipments = equipments;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }
    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("我叫").append(getName()).append(",我的等級：").append(getLevel());
        return stringBuffer.toString();
    }
}
