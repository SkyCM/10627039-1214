package Game;

public class Chef extends Roles {
    public Chef(String name){
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("Chef名字是"+getName()+"，我拿平底鍋敲人");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("Chef名字是"+getName()+"，我拿"+weapon+"敲人");
    }

    @Override
    public void doMagic() {

    }

    @Override
    public void doMagic(String magic) {

    }

    @Override
    public void doSpecialAttack() {
        System.out.println("Chef名字是"+getName()+"，我拿熱湯撒向敵人");
    }
}
