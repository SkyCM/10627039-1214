package Game;

public class Warrior extends Roles {
    public Warrior(String name){
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("Warrior名字是"+getName()+"，我用魔法滅人");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("Warrior名字是"+getName()+"，我用"+ weapon +"釋放出法術滅人");
    }

    @Override
    public void doMagic() {

    }

    @Override
    public void doMagic(String magic) {

    }

    @Override
    public void doSpecialAttack() {
        System.out.println("Warrior名字是"+getName()+"，我使用流星雨大法陣");
    }
}
